package com.assignment2.dijkstra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class DijkstrasShortestPath {

	/**
	 * @param args
	 */
	private ArrayList<AdjascentVertex>[] vertex;
	
	private int numberOfvertices;
	
	private List<Integer> exploredVertices;
	
	public int[] greedyScoreOfVertex;
	
	private boolean[] isAdded;
	
	private class AdjascentVertex{
		
		private int value; // vertex number
		private int edgeLength; // length of edge formed with another vertex
		private int dijkstraGreedyScore;
		
		AdjascentVertex(int value, int edgeLength){
			this.value = value;
			this.edgeLength = edgeLength;
			dijkstraGreedyScore = 0; 
		}
	}
	
	public DijkstrasShortestPath(FileReader file, int numberOfVertices) throws IOException{
		
		numberOfvertices = numberOfVertices;
		BufferedReader reader = new BufferedReader(file);
		String line;
		exploredVertices = new ArrayList<Integer>();
		vertex = new ArrayList[numberOfvertices+1];
		isAdded = new boolean[numberOfvertices+1];
		greedyScoreOfVertex = new int[numberOfvertices+1];
		for(int i = 0; i <= numberOfvertices; i++){
			vertex[i] = new ArrayList<AdjascentVertex>();
			greedyScoreOfVertex[i] = 0;
			isAdded[i] = false;
		}
		
		while((line = reader.readLine()) != null){
			line = line.trim().replaceAll("\t", " ");
			String[] rowElements = line.split(" ");
			int vertexIndex = Integer.parseInt(rowElements[0]);
			for(int i = 1; i < rowElements.length; i++){
				String[] eachAdjacentVertexInput = rowElements[i].split(",");
				// adding into vertex
				vertex[vertexIndex].add(new AdjascentVertex(Integer.parseInt(eachAdjacentVertexInput[0]), 
						Integer.parseInt(eachAdjacentVertexInput[1])));
			}
		}
	}
	
	public void shortestPathComputation(int sourceVertex){
		
		PriorityQueue<Integer> minPQ = new PriorityQueue<Integer>(1,scoreComparator());
		exploredVertices.add(sourceVertex);
		int recentlyAddedVertex = sourceVertex;
		isAdded[recentlyAddedVertex] = true;
		
		while(exploredVertices.size() != numberOfvertices){
			// add the adjacent vertices of the recentlyAddedVertex to priority queue
			minPQ = addValidVertices(minPQ, recentlyAddedVertex);
			recentlyAddedVertex = getRecentlyAdded(minPQ);
			
		}
	}
	
	private PriorityQueue<Integer> addValidVertices(PriorityQueue<Integer> minPQ, int recentlyAdded){
		
		for(AdjascentVertex adjVertex : vertex[recentlyAdded]){
			if(!isAdded[adjVertex.value]){
				if(!minPQ.contains(adjVertex.value)){
					greedyScoreOfVertex[adjVertex.value] = greedyScoreOfVertex[recentlyAdded] + adjVertex.edgeLength;
					minPQ.add(adjVertex.value);
				}else{
					int newScore = greedyScoreOfVertex[recentlyAdded] + adjVertex.edgeLength;
					if(newScore < greedyScoreOfVertex[adjVertex.value]){
						// remove the vertex and add again as the priority will change now
						minPQ.remove(adjVertex.value);
						greedyScoreOfVertex[adjVertex.value] = newScore;
						minPQ.add(adjVertex.value);
					}
				}
			}
		}
		return minPQ;
	}
	
	private int getRecentlyAdded(PriorityQueue<Integer> minPQ){
		int toBeAdded = minPQ.poll();
		exploredVertices.add(toBeAdded);
		isAdded[toBeAdded] = true;
		
		return toBeAdded;
	}
	public Comparator<Integer> scoreComparator(){
		return new ScorePriority();
	}
	
	private class ScorePriority implements Comparator<Integer>{
		
		public int compare(Integer vertex1, Integer vertex2){
			if(greedyScoreOfVertex[vertex1] > greedyScoreOfVertex[vertex2]){
				return +1;
			}else if (greedyScoreOfVertex[vertex1] < greedyScoreOfVertex[vertex2]){
				return -1;
			}else{
				return 0;
			}
		}

		
	}
	
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		// 7,37,59,82,99,115,133,165,188,197
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms2/Assignment2/DijkstraData.txt");
		DijkstrasShortestPath shortestPath = new DijkstrasShortestPath(file,200);
		shortestPath.shortestPathComputation(1);
		//System.out.println("blah");
		int[] shortestPathForVertices = {7,37,59,82,99,115,133,165,188,197};
		
		for(int i = 0 ; i < shortestPathForVertices.length; i++){
			System.out.print(shortestPath.greedyScoreOfVertex[shortestPathForVertices[i]] + ",");
		}
		
		
		
	}

}
