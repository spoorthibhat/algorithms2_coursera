package com.assignment3.median;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.PriorityQueue;

public class MedianMaintenance {

	/**
	 * @param args
	 */
	private PriorityQueue<Integer> highHeap;
	private PriorityQueue<Integer> lowHeap;
	private int sum;
	
	public MedianMaintenance(FileReader file) throws IOException{
		
		highHeap = new PriorityQueue<Integer>(); // min heap
		lowHeap = new PriorityQueue<Integer>(1, Collections.reverseOrder()); // max heap
		
		
		BufferedReader reader = new BufferedReader(file);
		String line = reader.readLine().trim();
		int firstNum = Integer.parseInt(line);
		int secondNum = Integer.parseInt(reader.readLine().trim());
		int median = firstNum; //m1
		sum = median;
		
		if(firstNum < secondNum){ // initializing the heaps
			lowHeap.add(firstNum);
			highHeap.add(secondNum);
		}else{
			lowHeap.add(secondNum);
			highHeap.add(firstNum);
		}
		
		while((line = reader.readLine()) != null){
			checkAndAdjustHeaps();
			median = getMedian();
			sum += median;
			int nextIncominNumber = Integer.parseInt(line.trim());
			addNextNumber(nextIncominNumber);
		}
		
		checkAndAdjustHeaps();
		median = getMedian();
		sum += median;
		System.out.println(sum%10000);
		
	}
	
	private void addNextNumber(int nextNumber){
		
		if(nextNumber < lowHeap.peek()){
			lowHeap.add(nextNumber);
		}else{
			highHeap.add(nextNumber);
		}
	}
	private int getMedian(){
		
		if(lowHeap.size() >= highHeap.size()){
			return lowHeap.peek();
		}else{
			return highHeap.peek();
		}
	}
	private void checkAndAdjustHeaps(){
		
		if(lowHeap.size() == highHeap.size() + 2){
			int toBeChanged = lowHeap.poll();
			highHeap.add(toBeChanged);
		}else if(highHeap.size() == lowHeap.size() + 2){
			int toBeChanged = highHeap.poll();
			lowHeap.add(toBeChanged);
		}else{
			return;
		}
	}
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms2/Assignment3/Median.txt");
		MedianMaintenance medianMaintenance = new MedianMaintenance(file);
		

	}

}
