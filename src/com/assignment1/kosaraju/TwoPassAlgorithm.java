package com.assignment1.kosaraju;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given the huge size of input, we need to run this with extra stack size and memory.
 * 
 * Command Line way (From home directory of this project (when you do ls, you should see "com" )
 * javac *.java
 * java -Xmx2048m -Xss8m com.assignment1.kosaraju.TwoPassAlgorithm
 * 
 * The above Xmx2048m allocates 2GM(2048MB) of memory space to run this program. Xss8m allocates 8MB of stack space to run this program.
 * 
 * @author spoorthi
 *
 */
public class TwoPassAlgorithm {
    public int[] nodeFinishTime;
    private int finishTimeCount ;
    private int numOfNodes;
    static Map< ArrayList<Integer>,Integer> scc;
    private int sizesOfSccs;
    Graph graph;

    public TwoPassAlgorithm(Graph inputgraph)
    {
        numOfNodes = inputgraph.numOfNodes();
        finishTimeCount = 0;
        nodeFinishTime = new int[numOfNodes];

        for(int i = 0;i < numOfNodes ; i++){
            nodeFinishTime[i] = 0;
        }
        scc = new HashMap< ArrayList<Integer>,Integer>();//list of nodes in SCC, size of SCC. 
        sizesOfSccs = 0;
        graph = inputgraph;
    }

    public void firstPass(Graph graph){
        for(int i = numOfNodes - 1; i >0; i-- ){
            if(!graph.isVisited(i)){
                firstPassDFS(i);
            }
        }
    }

    private void firstPassDFS(int startVertex) {
        graph.setVisited(startVertex);
        for (Integer neighborNode : graph.getNeighbor(startVertex)){
            if( !graph.isVisited(neighborNode)) {
                firstPassDFS(neighborNode);
            }
        }
        nodeFinishTime[startVertex] = finishTimeCount + 1;
        finishTimeCount++;
    }

    public void secondPass(Graph oldGraph){
        Graph finalGraph = new Graph(numOfNodes);

        for(int i = 1; i< numOfNodes ; i++){ // this would reverse graph after replacing old values with corresponding finish times.
            for(Integer x: oldGraph.getNeighbor(i)){
                finalGraph.addNode(nodeFinishTime[x], nodeFinishTime[i]);
            }
        }
        for(int node = numOfNodes-1 ;node >0 ;node--){
            if(!finalGraph.isVisited(node)){
                ArrayList<Integer> sccNodes = new ArrayList<Integer>();
                sizesOfSccs = 0;
                sccNodes = secondDfs(finalGraph,node,sccNodes);
                scc.put(sccNodes,sizesOfSccs);
            }
        }
    }

    private ArrayList<Integer> secondDfs(Graph secondgraph, int startNode, ArrayList<Integer> sccNodesValues){
        secondgraph.setVisited(startNode);
        sccNodesValues.add(startNode);
        sizesOfSccs++;
        for (int neighborNode: secondgraph.getNeighbor(startNode)){
            if(!secondgraph.isVisited(neighborNode)){
                secondDfs(secondgraph, neighborNode,sccNodesValues);
            }
        }
        return sccNodesValues;
    }

    public String fiveHighestSizeSccs(){
        String topFive = "";
        int[] sccSizes = new int[scc.size()];
        int i = 0;
        for(ArrayList<Integer> x: scc.keySet()){
            sccSizes[i] = scc.get(x);
            i++;
        }
        Arrays.sort(sccSizes);
        int highestSizeElement = sccSizes.length-1;
        int count = 0;
        for(int element = 0 ; element < 5; element++){
            topFive += Integer.toString(sccSizes[highestSizeElement]) + ",";
            highestSizeElement--;
            count++;
            if(highestSizeElement<0){
                break;
            }
        }
        if(count<5){
            while(count<5){
                topFive += "0,";
                count++;
            }
        }
        return topFive;
    }

    public static void main(String[] args) {
        //Step1: Construct a Graph
        Graph mygraph = new Graph(875715);
        try{
            BufferedReader br = new BufferedReader(new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms2/Assignment 1 test cases/scc.txt"));
            String line;
            while((line = br.readLine()) != null){
                String[] vertices = line.trim().split(" ");
                // add to graph by reversing the arcs
                mygraph.addNode(Integer.parseInt(vertices[1]), Integer.parseInt(vertices[0]));
            }
            System.out.println("Size:"+mygraph.adjacencyList.length);
            br.close();
        }catch(IOException e){
            System.out.println(e);
        }

        //Step-2: Run 2 pass of DFS.
        TwoPassAlgorithm alg = new TwoPassAlgorithm(mygraph);
        alg.firstPass(mygraph);
        alg.secondPass(mygraph);
        System.out.println(alg.fiveHighestSizeSccs());
    }
}