package com.assignment1.kosaraju;

import java.util.ArrayList;



public class Graph {

    private boolean[] visitedNodes;
    private int numberOfNodes;
    public ArrayList<Integer> adjacencyList[]; // tailNode is the index of the array and the data it contains are the head nodes corresponding to that tail node.

    public Graph(int numOfNodes) {
        numberOfNodes = numOfNodes;
        visitedNodes = new boolean[numOfNodes];
        adjacencyList = new ArrayList[numOfNodes];

        for ( int i = 0; i < numOfNodes; i++) {
            visitedNodes[i] = false; // initially all nodes will be marked as not visited.
            adjacencyList[i] = new ArrayList<Integer>();
        }
    }

    public ArrayList<Integer> getNeighbor(int vertex){
        return adjacencyList[vertex];
    }

    public int numOfNodes(){
        return numberOfNodes;
    }

    public void addNode(int tailNode, int headNode) {
        adjacencyList[tailNode].add(headNode);
    }

    public void setVisited(int vertex) {
        visitedNodes[vertex] = true;
    }

    public boolean isVisited(int vertex){
        return visitedNodes[vertex];
    }
}