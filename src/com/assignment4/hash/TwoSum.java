package com.assignment4.hash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TwoSum {

	/**
	 * @param args
	 */
	private TreeSet<Long> givenInput;
	private Set<Long> foundSums;
	private long lowestSum;
	private long highestSum;

	public TwoSum(FileReader file, int low, int high) throws IOException{

		BufferedReader reader = new BufferedReader(file);
		String line;

		givenInput = new TreeSet<Long>();
		foundSums = new HashSet<Long>();
		lowestSum = low;
		highestSum = high;

		while((line = reader.readLine()) != null){
			line = line.trim();
			givenInput.add(Long.parseLong(line));
		}
	}

	public void computation(){

		long range = highestSum - lowestSum + 1;
		for(long input: givenInput){

			long lowerPair = lowestSum - input;
			long higherPair = highestSum - input;

			TreeSet<Long> pairsSubset = new TreeSet<Long>();
            if(lowerPair > higherPair){
				long temp = lowerPair;
				lowerPair = higherPair;
				higherPair = temp;
			}
			Long ceiling = givenInput.ceiling(lowerPair);
			Long floor = givenInput.floor(higherPair);
			if(ceiling == null || floor==null || ceiling > floor){ // it means that there will be nothing in such range
				continue;
			}
			pairsSubset = (TreeSet) givenInput.subSet(ceiling, floor); // lowerPair inclusive & higherPair exclusive

			for(long pairOfInput: pairsSubset){
				if(pairOfInput != input){ // so that it does not consider itself
					foundSums.add(input + pairOfInput);
				}
			}
			foundSums.add(input + givenInput.floor(higherPair));
			if(foundSums.size() == range ){
				break;
			}
		}
		System.out.println("Number of sums found : " +foundSums.size());
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Long startTime = System.currentTimeMillis();
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms2/Assignment4/twoSum.txt"); // Ans: 427
		TwoSumNew twosum = new TwoSumNew(file, -10000,10000);
		twosum.computation();
		System.out.println("Time Taken:" + (System.currentTimeMillis() - startTime )/1000);
	}

}
